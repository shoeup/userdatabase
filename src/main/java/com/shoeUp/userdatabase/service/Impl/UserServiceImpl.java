package com.shoeUp.userdatabase.service.Impl;

import com.shoeUp.userdatabase.dto.MerchantDto;
import com.shoeUp.userdatabase.dto.UserDTO;
import com.shoeUp.userdatabase.entity.UserEntity;
import com.shoeUp.userdatabase.feign.FeignConfig;
import com.shoeUp.userdatabase.repository.UserRepository;
import com.shoeUp.userdatabase.service.UserService;
import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

//    @Autowired
//    PasswordEncoder passwordEncoder;
    @Autowired
    FeignConfig feignConfig;

    @Override
    public String addUser(UserDTO userDTO) {
        if(Objects.nonNull(userRepository.findByEmail(userDTO.getEmail())))
            return "Your Email Already Exists";

        UserEntity newUser = new UserEntity();
//        UserDTO userDTO = new UserDTO();
//        newUser.setName(userEntity.getName());
//        newUser.setEmail(userEntity.getEmail());
//        newUser.setRoleType(userEntity.getRoleType());
//        newUser.setPassword();
        BeanUtils.copyProperties(userDTO, newUser);
        userRepository.save(newUser);
        MerchantDto merchantDto = new MerchantDto();
        BeanUtils.copyProperties(newUser, merchantDto);


        if(newUser.getRoleType()==2)
            feignConfig.entry(merchantDto);
        return "Accepted";

    }

    @Override
    public List<UserEntity> getUser() {
        return userRepository.findAll();
    }

//    @Override
//    public Boolean checkUser(String email) {
//        if ()
//        return null;
//    }


}
