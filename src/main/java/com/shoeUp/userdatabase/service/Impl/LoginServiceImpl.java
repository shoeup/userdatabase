package com.shoeUp.userdatabase.service.Impl;

import com.shoeUp.userdatabase.entity.JsonEntity;
import com.shoeUp.userdatabase.entity.LoginEntity;
import com.shoeUp.userdatabase.entity.UserEntity;
import com.shoeUp.userdatabase.repository.TokenRepo;
import com.shoeUp.userdatabase.repository.UserRepository;
import com.shoeUp.userdatabase.service.LoginService;
import com.shoeUp.userdatabase.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    TokenService tokenService;

//    @Autowired
//    TokenRepo tokenRepo;

    @Override
    public JsonEntity searchByPassword(LoginEntity logInEntity) throws NullPointerException{
        UserEntity res = userRepository.findByEmail(logInEntity.getEmail());
        JsonEntity jsonEntity = new JsonEntity();
        if(res==null){
            jsonEntity.setAcceptance(0);
            jsonEntity.setRole(0);
            jsonEntity.setEmail("Invalid User");
            jsonEntity.setToken(-1);
            jsonEntity.setName("Not Found");
            return jsonEntity;
        }
        UserEntity userEntity = new UserEntity();
        userEntity = userRepository.findByEmail(logInEntity.getEmail());
//        JsonEntity jsonEntity = new JsonEntity();
//        if(!(logInEntity.getEmail().equals(tokenRepo.findByEmail(logInEntity.getEmail())))){
//            jsonEntity.setAcceptance(0);
//            jsonEntity.setRole(0);
//            jsonEntity.setEmail("Already Logged in");
//            jsonEntity.setToken(-1);
//            return jsonEntity;
//        }
            if(logInEntity.getPassword().equals(userEntity.getPassword())){
                jsonEntity.setAcceptance(1);
                int role_type = Math.toIntExact(userEntity.getRoleType());
                jsonEntity.setRole(role_type);
                jsonEntity.setToken(tokenService.giveToken(logInEntity.getEmail()));
                jsonEntity.setEmail(logInEntity.getEmail());
                jsonEntity.setId(userEntity.getId());
                jsonEntity.setName(userEntity.getName());
                return jsonEntity;
            }
                jsonEntity.setAcceptance(0);
                jsonEntity.setRole(0);
                jsonEntity.setEmail("Invalid User");
                jsonEntity.setToken(-1);
                jsonEntity.setName("Not Found");
                return jsonEntity;
    }
}