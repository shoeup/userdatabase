package com.shoeUp.userdatabase.service.Impl;

import com.shoeUp.userdatabase.entity.JsonEntity;
import com.shoeUp.userdatabase.entity.Token;
import com.shoeUp.userdatabase.repository.TokenRepo;
import com.shoeUp.userdatabase.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TokenServiceImpl implements TokenService {


    @Autowired
    TokenRepo tokenRepo;

    public float giveToken(String email){

        Token token = new Token();
        Long rand = (long) (Math.random()*9999);
        token.setTokenId(rand);
        token.setEmail(email);
        tokenRepo.save(token);
        return token.getTokenId();

    }

    @Override
    public Boolean valid(JsonEntity jsonEntity) {
        List<Token> list= tokenRepo.findAll();
        for(int i=0; i<list.size(); i++)
        {
            Token token = list.get(i);
            if(jsonEntity.getToken() == token.getTokenId()){
                return true;
            }
        }
        return false;
    }

    @Override
    public Boolean remove(JsonEntity jsonEntity) {

        List<Token> list= tokenRepo.findAll();
        for(int i=0; i<list.size(); i++)
        {
            Token token = list.get(i);
            if(jsonEntity.getToken() == token.getTokenId()){
                tokenRepo.delete(token);
                return true;
            }
        }
        return false;
    }


}
