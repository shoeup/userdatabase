package com.shoeUp.userdatabase.service;

import com.shoeUp.userdatabase.entity.JsonEntity;
import com.shoeUp.userdatabase.entity.Token;
import com.sun.org.apache.xpath.internal.operations.Bool;

public interface TokenService {
    public float giveToken(String email);

    Boolean valid(JsonEntity jsonEntity);

    Boolean remove(JsonEntity jsonEntity);
}
