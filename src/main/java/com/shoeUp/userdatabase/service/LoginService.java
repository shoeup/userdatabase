package com.shoeUp.userdatabase.service;

import com.shoeUp.userdatabase.entity.JsonEntity;
import com.shoeUp.userdatabase.entity.LoginEntity;

import java.util.List;

public interface LoginService {
    JsonEntity searchByPassword(LoginEntity logInEntity);
}
