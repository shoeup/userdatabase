package com.shoeUp.userdatabase.service;

import com.shoeUp.userdatabase.dto.UserDTO;
import com.shoeUp.userdatabase.entity.UserEntity;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import java.util.List;

@Service
public interface UserService{
    String addUser(UserDTO userDTO);

    List<UserEntity> getUser();

//    Boolean checkUser(String email);
}
