package com.shoeUp.userdatabase.controller;
import com.shoeUp.userdatabase.dto.UserDTO;
import com.shoeUp.userdatabase.entity.JsonEntity;
import com.shoeUp.userdatabase.entity.LoginEntity;
import com.shoeUp.userdatabase.service.LoginService;
import com.shoeUp.userdatabase.service.TokenService;
import com.shoeUp.userdatabase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    LoginService loginService;

    @Autowired
    TokenService tokenService;


    @CrossOrigin(origins = "http://172.16.29.129:3000")
    @PostMapping(value = "/add")
    ResponseEntity<String> addUser(@RequestBody UserDTO userDTO){
        ResponseEntity<String> responseEntity = new ResponseEntity<>(userService.addUser(userDTO), HttpStatus.ACCEPTED);
        return responseEntity;
    }
//    @GetMapping(value = "/show")
//    public ResponseEntity<List<UserEntity>> getUser(){
//        ResponseEntity<List<UserEntity>> response = new ResponseEntity<>(userService.getUser(), HttpStatus.ACCEPTED);
//        return response;
//    }
    @PostMapping(value = "/login")
    ResponseEntity<JsonEntity> check(@RequestBody LoginEntity logInEntity){
        ResponseEntity<JsonEntity> responseEntity = new ResponseEntity<>(loginService.searchByPassword(logInEntity), HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping(value = "/validation")
    ResponseEntity<Boolean> validate(@RequestBody JsonEntity jsonEntity){
        ResponseEntity<Boolean> validation = new ResponseEntity<>(tokenService.valid(jsonEntity), HttpStatus.ACCEPTED);
        return validation;
    }

    @GetMapping(value = "/logout")
    ResponseEntity<Boolean> logout(@RequestBody JsonEntity jsonEntity){
        ResponseEntity<Boolean> log = new ResponseEntity<>(tokenService.remove(jsonEntity), HttpStatus.ACCEPTED);
        return log;
    }

}
