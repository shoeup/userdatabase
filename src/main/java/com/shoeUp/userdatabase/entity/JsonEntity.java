package com.shoeUp.userdatabase.entity;

import lombok.Data;

import javax.persistence.Entity;

@Data
public class JsonEntity {

    Long id;
    int acceptance;
    int role;
    String name;
    String email;
    float token;

}
