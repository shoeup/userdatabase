package com.shoeUp.userdatabase.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "token_table")
public class Token {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @Column(name = "email")
    String email;
    @Column(name = "token_id")
    Long tokenId;
}
