package com.shoeUp.userdatabase.entity;

import lombok.*;

import javax.persistence.*;

@Entity(name = "user_table")

@Data

public class UserEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @Column(name = "name")
    String name;
    @Column(name = "email")
    String email;
    @Column(name = "password")
    String password;
    @Column(name = "role_type")
    Long roleType;

}
