package com.shoeUp.userdatabase.entity;

import lombok.Data;

@Data
public class LoginEntity {
    String email;
    String password;
}
