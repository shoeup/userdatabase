package com.shoeUp.userdatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class UserdatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserdatabaseApplication.class, args);
	}

}
