package com.shoeUp.userdatabase.repository;

import com.shoeUp.userdatabase.entity.LoginEntity;
import com.shoeUp.userdatabase.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByEmail(String email);//Select*employee where firstName = "abc";
    LoginEntity searchByPassword(String password);

    List<UserEntity> findByRoleType(Long roleType);
}
