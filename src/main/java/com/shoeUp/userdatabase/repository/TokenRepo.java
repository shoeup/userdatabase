package com.shoeUp.userdatabase.repository;

import com.shoeUp.userdatabase.entity.Token;
import com.shoeUp.userdatabase.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepo extends JpaRepository<Token, Long> {
//    String getByEmail(String email);
//    Float getByToken(float token);
//    void deleteByTokenId(Long tokenId);
    Token findByEmail(String email);

}
