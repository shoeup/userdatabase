package com.shoeUp.userdatabase.feign;

import com.shoeUp.userdatabase.dto.MerchantDto;
import com.shoeUp.userdatabase.entity.UserEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

@org.springframework.cloud.openfeign.FeignClient(value = "feignDemo", url="http://172.16.28.120:8080")
public interface FeignConfig {
 @PostMapping("/addmerchantfromdb")
     Boolean entry(MerchantDto merchantDto);


}
